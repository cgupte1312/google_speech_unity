﻿//	Copyright (c) 2016 steele of lowkeysoft.com
//        http://lowkeysoft.com
//
//	This software is provided 'as-is', without any express or implied warranty. In
//	no event will the authors be held liable for any damages arising from the use
//	of this software.
//
//	Permission is granted to anyone to use this software for any purpose,
//	including commercial applications, and to alter it and redistribute it freely,
//	subject to the following restrictions:
//
//	1. The origin of this software must not be misrepresented; you must not claim
//	that you wrote the original software. If you use this software in a product,
//	an acknowledgment in the product documentation would be appreciated but is not
//	required.
//
//	2. Altered source versions must be plainly marked as such, and must not be
//	misrepresented as being the original software.
//
//	3. This notice may not be removed or altered from any source distribution.
//
//  =============================================================================
//
// Acquired from https://github.com/steelejay/LowkeySpeech
//
using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Web;
using SimpleJSON;

[RequireComponent (typeof (AudioSource))]

public class GoogleVoice : MonoBehaviour {

	//public GUIText TextBox;

	struct ClipData
	{
		public int samples;
	}

	const int HEADER_SIZE = 44;

	private int minFreq;
	private int maxFreq;

	private bool micConnected = false;

	//A handle to the attached AudioSource
	private AudioSource goAudioSource;

	//public string apiKey;
	public float speed = 1;
	public float drag;
	private Rigidbody rb;
	public float x;
	public float y;

	// Use this for initialization
	void Start () {
		//Check if there is at least one microphone connected
		if(Microphone.devices.Length <= 0)
		{
			//Throw a warning message at the console if there isn't
			//Debug.LogWarning("Microphone not connected!");
		}
		else //At least one microphone is present
		{
			//Set 'micConnected' to true
			micConnected = true;

			//Get the default microphone recording capabilities
			Microphone.GetDeviceCaps(null, out minFreq, out maxFreq);

			//According to the documentation, if minFreq and maxFreq are zero, the microphone supports any frequency...
			if(minFreq == 0 && maxFreq == 0)
			{
				//...meaning 44100 Hz can be used as the recording sampling rate
				maxFreq = 44100;
			}

			//Get the attached AudioSource component
			goAudioSource = this.GetComponent<AudioSource>();
			rb = GetComponent<Rigidbody> ();

		}
	}

	void OnGUI() 
	{
		//If there is a microphone
		if(micConnected)
		{
			//If the audio from any microphone isn't being recorded
			if(!Microphone.IsRecording(null))
			{
				if (Input.GetKeyDown(KeyCode.Space)) 
				{
					//Start recording and store the audio captured from the microphone at the AudioClip in the AudioSource
					Debug.Log ("Recording!");
					goAudioSource.clip = Microphone.Start (null, true, 20, 44100); //Currently set for a 7 second clip

				}
			}
			else //Recording is in progress
			{
				if (Input.GetKeyDown(KeyCode.Return))
				//if(!goAudioSource.clip == false)
				{
					//Case the 'Stop and Play' button gets pressed
					Debug.Log("Testing!");
					float filenameRand = UnityEngine.Random.Range (0.0f, 10.0f);

					string filename = "testing" + filenameRand;

					Microphone.End(null); //Stop the audio recording

					//Debug.Log( "Recording Stopped");

					if (!filename.ToLower().EndsWith(".wav")) {
						filename += ".wav";
					}

					var filePath = Path.Combine("testing/", filename);
					filePath = Path.Combine(Application.dataPath, filePath);
					//Debug.Log("Created filepath string: " + filePath);

					// Make sure directory exists if user is saving to sub dir.
					Directory.CreateDirectory(Path.GetDirectoryName(filePath));
					SavWav.Save (filePath, goAudioSource.clip); //Save a temporary Wav File
					//Debug.Log( "Saving @ " + filePath);
					//Insert your API KEY here.
					string apiURL = "https://speech.googleapis.com/v1/speech:recognize?&key=AIzaSyB4OB7eNuxAabkp7GHJRAOY01PsZ6wQGWM";
					string Response;

					//Debug.Log( "Uploading " + filePath);
					Response = HttpUploadFile (apiURL, filePath, "file", "audio/wav; rate=44100");
					//Debug.Log ("Response String: " +Response);

					var jsonresponse = SimpleJSON.JSON.Parse(Response);

					if (jsonresponse != null) {		
						var transcripts = jsonresponse["results"][0]["alternatives"][0]["transcript"].Value;
						Debug.Log (jsonresponse);
						Debug.Log (transcripts);
						//TextBox.text = transcripts;

						//string transcript = "down";

						switch (transcripts)
						{
							case "up":
								y += speed;
								break;
							case "down":
								y -= speed;
								break;
							case "left":
								x -= speed;
								break;
							case "right":
								x += speed;
								break;
						}

						//Vector3 initialPosition = rb.position;
						Vector3 position = new Vector3(x, 0.0f, y);
						rb.AddForce(position * drag);

					}
					//goAudioSource.Play(); //Playback the recorded audio

					File.Delete(filePath); //Delete the Temporary Wav file
				}
			}
		}
		else // No microphone
		{
			Debug.Log("Microphone not connected!");
		}
	}



		

	public string HttpUploadFile(string url, string file, string paramName, string contentType) {

		System.Net.ServicePointManager.ServerCertificateValidationCallback += (o, certificate, chain, errors) => true;
		//Debug.Log(string.Format("Uploading {0} to {1}", file, url));

		Byte[] bytes = File.ReadAllBytes(file);
		String file64 = Convert.ToBase64String(bytes,
			Base64FormattingOptions.None);

		//Debug.Log(file64);



		try
		{

			var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
			httpWebRequest.ContentType = "application/json";
			httpWebRequest.Method = "POST";

			using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
			{




				string json = "{ \"config\": { \"languageCode\" : \"en-US\" }, \"audio\" : { \"content\" : \"" + file64 + "\"}}";

				//Debug.Log(json);
				streamWriter.Write(json);
				streamWriter.Flush();
				streamWriter.Close();
			}

			var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
			//Debug.Log(httpResponse);

			using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
			{
				var result = streamReader.ReadToEnd();
				return result;
			}

		} catch (WebException ex) {
			//var resp = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
			Debug.Log(ex);

		}
			
		return "empty";

	}

}

